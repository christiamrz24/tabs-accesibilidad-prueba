$(document).ready(function() {
    $("#lbl-1").click(function () {
        $("#lbl-1").addClass("bg-colorlbl"); //Añadimos la clase al lbl 1
        $("#lbl-2, #lbl-3, #lbl-4").removeClass("bg-colorlbl"); //Quitamos la clase a los demas lbl
    });
    $("#lbl-2").click(function () {
        $("#lbl-2").addClass("bg-colorlbl"); //Añadimos la clase al lbl 2
        $("#lbl-1, #lbl-3, #lbl-4").removeClass("bg-colorlbl");
    });
    $("#lbl-3").click(function () {
        $("#lbl-3").addClass("bg-colorlbl"); //Añadimos la clase al lbl 3
        $("#lbl-1, #lbl-2, #lbl-4").removeClass("bg-colorlbl");
    });
    $("#lbl-4").click(function () {
        $("#lbl-4").addClass("bg-colorlbl"); //Añadimos la clase al lbl 4
        $("#lbl-1, #lbl-2, #lbl-3").removeClass("bg-colorlbl");
    });
});
function mostrar(id){ //Pedimos un id
    if (id == 1){
        document.querySelector('#radio-1').checked = true; //Activamos el radio button
        $("#lbl-1").addClass("bg-colorlbl"); //Agregamos la clase
        $("#lbl-2, #lbl-3, #lbl-4").removeClass("bg-colorlbl"); //Quitamos las demas clases
    } else if (id == 2){
        document.querySelector('#radio-2').checked = true;
        $("#lbl-2").addClass("bg-colorlbl");
        $("#lbl-1, #lbl-3, #lbl-4").removeClass("bg-colorlbl");
    } else if (id == 3){
        document.querySelector('#radio-3').checked = true;
        $("#lbl-3").addClass("bg-colorlbl");
        $("#lbl-1, #lbl-2, #lbl-4").removeClass("bg-colorlbl");
    } else if (id == 4){
        document.querySelector('#radio-4').checked = true;
        $("#lbl-4").addClass("bg-colorlbl");
        $("#lbl-1, #lbl-2, #lbl-3").removeClass("bg-colorlbl");
    }
}